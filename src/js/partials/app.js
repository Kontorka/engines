//Some JS

$('.single-item').slick({
    dots: true,
    autoplay: false,
    autoplaySpeed: 3000,
    prevArrow: false,
    nextArrow: false
});


// $('.slider-for').slick({
//     slidesToShow: 1,
//     slidesToScroll: 1,
//     arrows: false,
//     fade: true,
//     asNavFor: '.slider-nav'
// });
// $('.slider-nav').slick({
//     slidesToShow: 3,
//     slidesToScroll: 1,
//     asNavFor: '.slider-for',
//     dots: false,
//     centerMode: false,
//     focusOnSelect: true,
//     infinite: false
// });

$('.header__sandwich').on('click', function() {

    if($(this).hasClass('header__sandwich_open')) {
        $(this).removeClass('header__sandwich_open');
        $('.top-menu').removeClass('top-menu-open');
    } else {
        $(this).addClass('header__sandwich_open');
        $('.top-menu').addClass('top-menu-open');
    }

});


$('.popup-open').on('click', function() {
    $('.popup__overlay').addClass('popup__overlay_active');
    $('.popup').addClass('popup_active');
});

$('.popup__close').on('click', function() {
    $('.popup__overlay').removeClass('popup__overlay_active');
    $('.popup').removeClass('popup_active');
});
//
// $('.popup__overlay').on('click', function() {
//     $('.popup__overlay').removeClass('popup__overlay_active');
//     $('.popup').removeClass('popup_active');
// });

$(window).on('scroll', function (scroll) {

    let $header = $('.header');

    // console.log($(this).scrollTop());

    if ($(this).scrollTop() > 34) {
        $header.addClass('header_mini');

        if (window.matchMedia("(max-width: 2560px)").matches) {
            // $header.find('.logo').attr('src', '/img/logo_min.png')
        }

    } else {
        $header.removeClass('header_mini');

        if (window.matchMedia("(max-width: 2560px)").matches) {
            // $header.find('.logo').attr('src', 'img/logo.png');
        }
    }
});


jQuery(function(){

    window.onresize = function(event) {

    };

    if(window.innerWidth < 421) {

        var minimized_elements = $('#content p');
        var counter = 1;
        minimized_elements.each(function(){
            if(counter==1){
                $(this).addClass("first");
                var t = $(this).text();

                $(this).html(
                    t.slice(0,t.length)+'<span>... </span><a href="#" class="read-more-link more-close">Подробнее</a>'+
                    '<span style="display:none;">'+ t.slice(t.length,t.length)+' <a href="#" class="read-more-link more-open">Скрыть</a></span>'
                );
            } else{
                $(this).hide();
            }
            counter++;
        });

        $('a.more-close', minimized_elements).click(function(event){
            event.preventDefault();
            $(this).hide().prev().hide();
            $(this).next().show();
            $('p').show();
        });

        $('a.more-open', minimized_elements).click(function(event){
            event.preventDefault();
            $(this).parent().hide().prev().show().prev().show();
            $('p').not('.first').hide();
        });
    }
});



