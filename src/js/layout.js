
$('.js-layoutMain').on('click', function (event) {

	var $target = $(event.target),
		attrTarget = $target.attr('data-scroll-to'),
		attrTab = $target.attr('data-tab-target'),
		attrForm = $target.attr('data-form');

	if (attrTarget) {

		$('html, body').animate({
			scrollTop: $('[data-id="' + attrTarget + '"]').offset().top - 50
		}, 500);

		$('.header__sandwich').removeClass('header__sandwich_open');
		$('.nav__sandwich').removeClass('nav__sandwich_open');
		$('.nav__sublinkOverlay').removeClass('nav__sublinkOverlay_active');
		$('.nav__sublinkWrapper').removeClass('nav__sublinkWrapper_open');
		$('.nav').removeClass('nav_active');
	}
	if ($target.hasClass('header__sandwich')) {

		$target.toggleClass('header__sandwich_open');
		$('.nav').toggleClass('nav_active');

	}
	if ($target.hasClass('nav__link_openSubmenu') || $target.hasClass('nav__sandwich')) {

		$('.nav__sandwich').toggleClass('nav__sandwich_open');
		//$('.nav').addClass('nav_open');
		$('.nav__sublinkOverlay').toggleClass('nav__sublinkOverlay_active');
		$('.nav__sublinkWrapper').toggleClass('nav__sublinkWrapper_open');

	}
	if ($target.hasClass('nav__sublinkClose') || $target.hasClass('nav__sublinkOverlay')) {

		$('.nav__sandwich').removeClass('nav__sandwich_open');
		$('.nav__sublinkOverlay').removeClass('nav__sublinkOverlay_active');
		$('.nav__sublinkWrapper').removeClass('nav__sublinkWrapper_open');

	}
	if (attrTab) {
		$('.nav__sublinkOverlay').removeClass('nav__sublinkOverlay_active');
		$('.nav__sublinkWrapper').removeClass('nav__sublinkWrapper_open');
		$('.tabs__tab:not(:has(.tabs__tab_open))').removeClass('tabs__tab_open');
		$('.tabs__content:not([data-tab-content="' + attrTab + '"])').css('display', 'none');
		$('.tabs__tab[data-tab-target="' + attrTab + '"]').addClass('tabs__tab_open');
		$('.tabs__content[data-tab-content="' + attrTab + '"]').slideDown(500);

		$('html, body').animate({
			scrollTop: $('[data-tab-content="' + attrTab + '"]').offset().top - 150
		}, 500);

		if (window.matchMedia("(max-width: 1169px)").matches) {

			$('[data-tab-content="' + attrTab + '"]').find('.js-slider-1').slick('refresh');
		}

		if (window.matchMedia("(min-width: 1170px)").matches) {

			setTimeout(function () {

				$('.tabs').css('min-height', $('.tabs__content[data-tab-content="' + attrTab + '"]').outerHeight() + 200)

			}, 550)
		}
	}
	if (attrForm) {
		$('.popup__overlay').addClass('popup__overlay_active');
		$('.popup').addClass('popup_active');
	}
	if ($target.hasClass('popup__close') || $target.hasClass('popup__overlay')) {
		$('.popup__overlay').removeClass('popup__overlay_active');
		$('.popup').removeClass('popup_active');
	}


});

//Убриаем баг на iphone - при скролле срабатывает location.reload()
jQuery(document).ready(function ($) {

	/* Store the window width */
	var windowWidth = $(window).width();

	/* Resize Event */
	$(window).resize(function () {
		// Check if the window width has actually changed and it's not just iOS triggering a resize event on scroll
		if ($(window).width() != windowWidth) {

			// Update the window width for next time
			windowWidth = $(window).width();

			// Do stuff here

			//При изменении размера окна - перезагружаем страницы (избегая конфликтов скриптов)

			//location.href = location.href Костыль для localhost
			location.reload(false);

		}
	});

});

$(document).mousemove(function (e) {
	var x = (e.pageX * 1 / 100),
		transformX = 'translateX(' + x + 'px)';
	$('.banner__img').css('transform', transformX);
});

$(window).on('scroll', function () {

	$header = $('.header');

	if ($header.offset().top > 10) {
		$header.addClass('header_mini');


		if (window.matchMedia("(min-width: 1170px)").matches) {
			$header.find('.logo__img').attr('src', 'img/logo_mini.svg')
		}

	} else {
		$header.removeClass('header_mini');

		if (window.matchMedia("(min-width: 1170px)").matches) {
			$header.find('.logo__img').attr('src', 'img/logo.svg');
		}
	}
});


var upload = document.querySelector('.form__file');

function getFileNames() {

	return Array.prototype.map.call(upload.files, function (file) {
		return file.name;
	});
}

upload.onchange = function () {

	var funcResult = getFileNames();

	document.querySelector('.form__textFile').innerHTML = funcResult;
}

$('.js-form').submit(function (e) {
	e.preventDefault();
	var $that = $(this),
		formData = new FormData($that.get(0));

	$.ajax({
		url: "form.php",
		type: 'POST',
		contentType: false,
		processData: false,
		data: formData,
		success: function (response) {
			console.log(response);

			if (response === 'Вы робот' || response === 'Валидация не пройдена') {

				alert('Вы не прошли проверку reCaptcha, попробуйте еще раз')
			} else {
				$('.form__success').addClass('form__success_active');
			}

		},
		error: function (response) {
			console.log(response);
		}
	});
	return false;
});
